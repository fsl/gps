// Declarations of classes and functions for the
// gps utility. The gps utility will determine a
// set of isotropic directions on the sphere.
//
// gps.h
//
// Jesper Andersson, FMRIB Image Analysis Group
//
// Copyright (C) 2011 University of Oxford
//

#ifndef gps_h
#define gps_h

#include <memory>
#include <cstdlib>
#include <string>
#include <vector>
#include <cmath>
#include "armawrap/newmat.h"
#include "utils/options.h"
#include "miscmaths/miscmaths.h"

#ifndef TicToc
#include <sys/time.h>
#define TicToc(task) { timeval tim;		\
  gettimeofday(&tim,NULL); \
  task; \
  timeval tim2; \
  gettimeofday(&tim2,NULL); \
  cout << "Call to " #task " took " << 1000000*(tim2.tv_sec-tim.tv_sec) + tim2.tv_usec - tim.tv_usec << " usec" << endl; }
#endif

namespace GPS {

class GpsException: public std::exception
{
private:
  std::string m_msg;
public:
  GpsException(const std::string& msg) throw(): m_msg(msg) { std::cout << what() << std::endl; }

  virtual const char * what() const throw() {
    return std::string("gps: msg=" + m_msg).c_str();
  }

  ~GpsException() throw() {}
};

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//
// Class Bvec
//
// This class manages a single diffusion direction. It utilies a dual
// representation in cartesian and spherical coordinates and these
// are kept in accordance at all times. It also supplies the partial
// derivatives of the cartesion represenation w.r.t. to the angles
// of the spherical representation.
// scan.
//
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

class Bvec
{
public:
  Bvec(); // Initialises to a random vector on the unity sphere
  Bvec(const NEWMAT::ColumnVector& vec);
  void SetAngles(const NEWMAT::ColumnVector& angl);
  void SetVector(const NEWMAT::ColumnVector& vec);
  void SetSubset(unsigned int ss) { _ss=ss; }
  void SetRandomVector();
  void SignSwap();
  unsigned int Subset() const { return(_ss); }
  const NEWMAT::ColumnVector& Vector() const { return(_vr); }
  const NEWMAT::ColumnVector& Angles() const { return(_ar); }
  const NEWMAT::ColumnVector& dVec_dtheta() const { if (!_dutd) update_derivatives(); return(_dvdt); }
  const NEWMAT::ColumnVector& dVec_dphi() const { if (!_dutd) update_derivatives(); return(_dvdp); }
  const NEWMAT::ColumnVector& d2Vec_dtheta2() const { if (!_dutd) update_derivatives(); return(_d2vdt2); }
  const NEWMAT::ColumnVector& d2Vec_dtheta_dphi() const { if (!_dutd) update_derivatives(); return(_d2vdtdp); }
  const NEWMAT::ColumnVector& d2Vec_dphi2() const { if (!_dutd) update_derivatives(); return(_d2vdp2); }

private:
  NEWMAT::ColumnVector _vr;                // Vector representation
  NEWMAT::ColumnVector _ar;                // Spherical angles representation
  unsigned int         _ss;                // Sub-set identifier
  mutable NEWMAT::ColumnVector _dvdt;      // Partial derivative of vector representation w.r.t. theta
  mutable NEWMAT::ColumnVector _dvdp;      // Partial derivative of vector representation w.r.t. phi
  mutable NEWMAT::ColumnVector _d2vdt2;    // 2nd derivative, see above
  mutable NEWMAT::ColumnVector _d2vdp2;    // 2nd derivative, see above
  mutable NEWMAT::ColumnVector _d2vdtdp;   // 2nd derivative, see above
  mutable bool                 _dutd;      // Indicates if Derivatives are Up To Date

  static double PI;

  void set_ar_from_vr();
  void set_vr_from_ar();
  void update_derivatives() const;
};

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//
// Class BvecCollection
//
// This class manages a set of Bvecs. The first of these should
// always be [1 0 0] and the remaining can take any (unity norm)
// values. Its methods supplies the total Coulomb forces (cf) of a
// system of positive charges at both ends of all directions. It
// also  supplies the gradient and the Hessian of the cf w.r.t.
// to all "free" angles, noting that the first direction is fixed to
// [1 0 0].
//
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

class BvecCollection
{
public:
  BvecCollection(unsigned int nvec);
  BvecCollection(const std::vector<unsigned int>& ss, double ssw);
  BvecCollection(NEWMAT::Matrix mat);
  unsigned int NBvec() const { return(_bv.size()); }
  unsigned int NBvec(unsigned int i) const { if (i>=_ss.size()) GpsException("BvecCollection::NBvec: index out of range"); return(_ss[i]); }
  unsigned int NPar() const { return(2*(_bv.size()-1)); }
  NEWMAT::ColumnVector GetPar() const;
  void SetPar(const NEWMAT::ColumnVector& p);
  unsigned int NumberOfSubsets() const { return(_ss.size()); }
  double SubsetWeight(unsigned int i) const { if (i>=_ss.size()) GpsException("BvecCollection::SubsetWeight: index out of range"); return(_ssw[i]); }
  void ShakeIt(unsigned int ns=50);
  void SwapIt(unsigned int ns=1);
  void OptimiseOnWholeSphere(unsigned int niter=50, bool verbose=false);
  const Bvec& GetBvec(unsigned int i) const { if (i>=NBvec()) GpsException("BvecCollection::GetBvec: index out of range"); return(_bv[i]); }
  void SetBvec(unsigned int i, const NEWMAT::ColumnVector& vec) {
    if (!i || i>=NBvec()) GpsException("BvecCollection::GetBvec: index out of range");
    _bv[i].SetVector(vec);
  }
  void SetBvecToRandom(unsigned int i) {
    if (!i || i>=NBvec()) GpsException("BvecCollection::GetBvec: index out of range");
    _bv[i].SetRandomVector();
  }
  NEWMAT::ReturnMatrix GetAllBvecs() const;
  NEWMAT::ReturnMatrix GetAllBvecs(unsigned int i) const;
  double CoulombForces() const;
  NEWMAT::ReturnMatrix CoulombForcesGradient() const;
  NEWMAT::ReturnMatrix CoulombForcesNumericalGradient();
  NEWMAT::ReturnMatrix CoulombForcesHessian() const;
  NEWMAT::ReturnMatrix CoulombForcesNumericalHessian();
  double SingleChargeCoulombForces() const;
private:
  std::vector<Bvec>           _bv;
  std::vector<unsigned int>   _ss;  // Number of vectors per subset
  std::vector<double>         _ssw; // Subset weights

  double sqr(double a) const { return(a*a); }
  void swap_groups(unsigned int i, unsigned int j) {
    unsigned int tmp = _bv[i].Subset(); _bv[i].SetSubset(_bv[j].Subset()); _bv[j].SetSubset(tmp);
  }
};

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//
// Class GpsCF
//
// This class is effectively a wrapper class for BvecCollection that
// is a sub-class of NonlinCF which means that it can be used with
// the non-linear optimisation methods in MISCMATHS.
//
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

class GpsCF: public MISCMATHS::NonlinCF
{
public:
  GpsCF(const BvecCollection& bc, bool verbose) : _bc(bc), _verbose(verbose) {}
  unsigned int NPar() const { return(_bc.NPar()); }
  BvecCollection GetBvecs() const { return(_bc); }

  // cost-function (Coloumb forces), gradient and Hessian as specified in NonlinCF class

  double cf(const NEWMAT::ColumnVector& p) const {
    if (p.Nrows() != int(NPar())) throw GpsException("GpsCF::cf: Mismatch between p and number of parameters");
    _bc.SetPar(p);
    double cfv = _bc.CoulombForces();
    if (_verbose) std::cout << "cf = " << cfv << std::endl;
    return(cfv);
  }
  NEWMAT::ReturnMatrix grad(const NEWMAT::ColumnVector& p) const {
    if (p.Nrows() != int(NPar())) throw GpsException("GpsCF::grad: Mismatch between p and number of parameters");
    _bc.SetPar(p); return(_bc.CoulombForcesGradient());
  }
  std::shared_ptr<MISCMATHS::BFMatrix> hess(const NEWMAT::ColumnVector& p,
					      std::shared_ptr<MISCMATHS::BFMatrix>  iptr=std::shared_ptr<MISCMATHS::BFMatrix>()) const
  {
    if (p.Nrows() != int(NPar())) throw GpsException("GpsCF::hess: Mismatch between p and number of parameters");
    _bc.SetPar(p); return(std::shared_ptr<MISCMATHS::BFMatrix>(new MISCMATHS::FullBFMatrix(_bc.CoulombForcesHessian())));
  }
private:
  mutable BvecCollection _bc;
  bool                   _verbose;
};

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//
// Class GpsCommandLineOptions
//
// This class parses the input to the gps utility.
//
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

class GpsCommandLineOptions {
public:
  GpsCommandLineOptions(int argc, char *argv[]);
  std::vector<std::string> OutFnames() const { return(_out_fnames); }
  std::string OutFname(unsigned int i) const {
    if (i >= _out_fnames.size()) GpsException("GpsCommandLineOptions::OutFname: Index out of range");
    return(_out_fnames[i]);
  }
  std::vector<unsigned int> NDir() const { return(_ndir); }
  unsigned int NSubSet() const { return(_ndir.size()); }
  unsigned int TotDir() const { return(_totdir); }
  double SubsetWeight() const { return(static_cast<double>(_ssw.value())); }
  bool OptimiseOnWholeSphere() const { return(_optws.value()); }
  bool Verbose() const { return(_verbose.value()); }
  bool Debug() const { return(_debug.value()); }
  bool HasInitMatrix() const { return((_init.value().size()) ? true : false); }
  NEWMAT::Matrix InitMatrix() const { return(_init_mat); }
  bool Report() const { return(_report.value()); }
private:
  std::string                           _title;
  std::string                           _examples;
  Utilities::Option<bool>               _verbose;
  Utilities::Option<bool>               _help;
  Utilities::Option<std::string>             _out;
  Utilities::Option<std::vector<int> >  _ndir_inp;
  Utilities::Option<float>              _ssw;
  Utilities::Option<bool>               _optws;
  Utilities::Option<int>                _ranseed;
  Utilities::HiddenOption<bool>         _debug;
  Utilities::Option<std::string>             _init;
  Utilities::Option<bool>               _report;
  NEWMAT::Matrix                        _init_mat;
  std::vector<unsigned int>             _ndir;
  unsigned int                          _totdir;
  std::vector<std::string>              _out_fnames;
};

} // End namespace GPS

#endif // End #ifndef gps_h
