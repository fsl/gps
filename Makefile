include ${FSLCONFDIR}/default.mk

PROJNAME = gps
XFILES   = gps
LIBS     = -lfsl-miscmaths -lfsl-NewNifti -lfsl-znz \
           -lfsl-cprob -lfsl-utils

all: ${XFILES}

gps: gps.o
	${CXX} -o $@ $^ ${LDFLAGS}
